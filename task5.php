<?php
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
  $name = $_POST['name'];
  // TODO: output personalized greeting message
  echo "Hello, $name! Welcome to our website!";
} else {
?>
<form method="POST">
  <label for="name">Enter your name:</label>
  <input type="text" id="name" name="name">
  <button type="submit">Submit</button>
</form>
<?php
}
?>